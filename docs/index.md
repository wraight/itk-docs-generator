# PDB Structures

Automatically generated pages on ITk PDB object structures: 

- Per ITk Project: structures for ITk Projects: Pixels, Strips, Common Mechanics and Common Electronics
- Others: non-project specific

Automated pages per component type:

- component type structure
- test type structures
- external links


__Component Type Structure__

For each component type:

- Stages: list of stages for component (inc. alternative and initial & final flags if defined)
- Types: sub-types of component type
- Properties: (non-header) properties of component type 
- Flags: list of possible flags for component type
- Relatives: list of (nearest) relatives


__Test Type Structure__

For each test type of component type:

- Stages: list of stages in which test type is used for component
- Properties: (non-header) properties of test type 
- Results: (non-header) results of test type 


__Xeno info.__

If possible, for each componentType collate information external to PDB:

- EDMS links
- Twiki links
- Other links

