### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.2.4
import pandas as pd
import argparse
# !{sys.executable} -m pip install itkdb==0.3.7
import itkdb
import itkdb.exceptions as itkX
import datetime
from datetime import datetime
from string import ascii_lowercase 
### other
cwd = os.getcwd()


###############
### read-in parts
###############

### standard inputs
def GetArgs(descStr=None):
    my_parser = argparse.ArgumentParser(description=descStr)
    # Add the arguments
    my_parser.add_argument('--projCode', '-p', type=str, help='project: CE, CM, S, P', required=True)
    my_parser.add_argument('--compType', '-ct', type=str, help="componentType (code)")
    args = my_parser.parse_args()
    return args

#################
### mapping: PDB and p_d_s
#################

crossDict={'P':
         [
            {'p_d_s':"pcbs", 'PDB':"PCB", 'twiki':None },
            {'p_d_s':"sensors", 'PDB':"SENSOR_TILE", 'twiki':None },
            {'p_d_s':"modules", 'PDB':"MODULE", 'twiki':"ITkPixelModules" },
            {'p_d_s':None, 'PDB':"FE_CHIP", 'twiki':"PixelFE-chip" }
         ],
         'S':
         [
            {'p_d_s':"pwbs", 'PDB':"PWB", 'twiki':"Powerboards" },
            {'p_d_s':"sensors", 'PDB':"SENSOR", 'twiki':"ITKstripsSensors" },
            {'p_d_s':"hybrids", 'PDB':"HYBRID_ASSEMBLY", 'twiki':None },
            {'p_d_s':"asics", 'PDB':"HCC", 'twiki':"ITKstripsSTARChips" },
            {'p_d_s':"modules", 'PDB':"MODULE", 'twiki':"ITKstripsSTARModules" },
            {'p_d_s':"stave", 'PDB':"STAVE", 'twiki':None },
            {'p_d_s':None, 'PDB':"BT", 'twiki':None }
         ],
         'CM':
         [
             {'p_d_s':"None", 'PDB':"None", 'twiki':"None" }
         ],
         'CE':
         [
             {'p_d_s':"None", 'PDB':"None", 'twiki':"None" }
         ]
        }


### map project code to name
projMap={'P':"pixels", 'S':"strips", 'CE':"common-electronics", 'CM':"common-mechanics"}

#################
### functions
#################


### intro bit
def Disclaimer():
    disStr=[]
    disStr.append("!!! notice")
    disStr.append(f"\tPage generated: {datetime.now().date()}")
    disStr.append("\tSee source code [here](https://gitlab.cern.ch/wraight/itk-docs-generator)")
    disStr.append("\tFor updates/fixes contact: wraightATcern.ch")
    return disStr

### intro bit
def PDSDisclaimer():
    disStr=[]
    disStr.append("!!! additional")
    disStr.append(f"\tMapping to production_database_scripts is developing")
    disStr.append("\tSee full repository [here](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts)")
    return disStr

### formatting for properties, stages and flags
def GetDictTable(compObj,keyName):

    colList=[]
    ### properties formatting
    if keyName=="properties":
        colList=['name','code','description','dataType','required']
    elif keyName=="stages":
        colList=['name','code','order','alternative','initial','final']
    elif keyName=="flags":
        colList=['name','code']
    elif keyName=="types":
        colList=['name','code','existing']
    ### batch Info
    elif keyName=="testTypes":
        colList=['name','code']
    elif keyName=="componentTypes":
        colList=['name','code','project','state','quantity']
    else:
        print(f"unknown keyName: {keyName}")
    
    df_tab=pd.DataFrame()
    try:
        df_tab=pd.DataFrame(compObj[keyName])
    except KeyError: 
        print(f"Missing key {keyName}")
        print(f" - keys available: {compObj.keys()}")

    if df_tab.empty:
        return f"No _{keyName}_ found for object"
    else:
        if len(df_tab.columns)<1:
            return f"No columns in object"
        useCols=set(colList).intersection(list(df_tab.columns))
        if len(useCols)<1:
            return f"No expected columns object"
        return df_tab[list(useCols)].to_markdown(index=False)


### mermaid formatting
def GetMermaidDiagram(stagesList):
    df_stages=pd.DataFrame(stagesList)

    merStr=[]
    merStr.append("```mermaid\nflowchart LR")

    ### layout stages
    for st in ["Production Stages","Final Stages"]:
        merStr.append(f"\tsubgraph {st}")
        if "final" in st.lower():
            df_iter=df_stages.query("final==True")
        else:
            df_iter=df_stages.query("final==False")
        for i,row in df_iter.iterrows():
            # print(f"this {i}: {row['code']}")
            # refStr=ascii_lowercase[i]
            refStr=row['code'].replace('/','__')
            if row['alternative']==True:
                merStr.append(f"\t\tsubgraph \"{row['name']} (alternative)\"")
            else:
                merStr.append(f"\t\tsubgraph \"{row['name']}\"")
            merStr.append(f"\t\t\t{refStr}1[\"code: {row['code']}\"]")
            merStr.append(f"\t\t\t{refStr}2[\"tests: ")
            for tt in row['testTypes']:
                try:
                    merStr.append(f"\t\t\t{tt['testType']['name']}")
                except KeyError:
                    pass
                except TypeError:
                    pass
            merStr.append("\t\t\t\"]")
            # print(row['testTypes'])
            merStr.append("\t\tend")
        merStr.append("\tend")

    # ### join stages (issue with itk-docs mermaid v8.8.0 )
    # df_iter=df_stages.query("final==False")
    # for i,rowi in df_iter.iterrows():
    #     for j,rowj in df_iter.iterrows():
    #         if j==i+1:
    #             merStr.append(f"{ascii_lowercase[i]}1-->{ascii_lowercase[j]}1")

    merStr.append("```")

    return merStr


### tests per stage formatting
def GetTestsPerStageTables(stagesList):
    df_stages=pd.DataFrame(stagesList)
    df_tests=df_stages.explode('testTypes').reset_index(drop=True)

    tabList=[]
    for st in sorted( list(df_tests['code'].unique()) ):
        if "*" in st:
            tabList.append(f"\n### Stage Code: __{st}__\n")
        else:
            tabList.append(f"\n### Stage Code: **{st}**\n")
        df_st=df_tests.query('code=="'+st+'"')
        for col in ['code','name']:
            df_st['test_'+col]=df_st['testTypes'].apply(lambda x: x['testType'][col] if type(x)==type({}) and "testType" in x.keys() else x)
        df_st['test_order']=df_st['testTypes'].apply(lambda x: x['order'] if type(x)==type({}) and "order" in x.keys() else x)
        # df_st=df_st.dropna()
        if df_st.empty:
            tabList.append("no stage information found")
        else:
            tabList.append(df_st[['test_name','test_code','test_order']].to_markdown(index=False))

    return tabList


### relatives formatting
def GetRelativeTables(relDict):
    tabList=[]
    if type(relDict)!=type({}):
        return []
    for k in sorted(relDict.keys()):
        if "*" in k:
            tabList.append(f"__type: {k}__")
        else:
            tabList.append(f"**type: {k}**")

        df_rel=pd.DataFrame(relDict[k])
        # get type if possible
        if "type" in df_rel.columns:
            df_rel['type']=df_rel['type'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else None)

        useCols=set(['name','code','type']).intersection(list(df_rel.columns))
        if len(useCols)<1:
            tabList.append("No table possible")
        else:
            tabList.append(df_rel[list(useCols)].to_markdown(index=False))
    
    return tabList


### testType formatting
def GetTestTypeTable(myClient,compInfo):
    if "stages" not in compInfo.keys():
        return ["no stage infromation found"]
    tabStr=[]
    df_stages=pd.DataFrame(compInfo['stages'])
    df_tests=df_stages.explode('testTypes').reset_index(drop=True)    
    for col in ['code','name']:
        df_tests['test_'+col]=df_tests['testTypes'].apply(lambda x: x['testType'][col] if type(x)==type({}) and "testType" in x.keys() else x)
    df_tests['test_order']=df_tests['testTypes'].apply(lambda x: x['order'] if type(x)==type({}) and "order" in x.keys() else x)
    df_tests=df_tests.rename(columns={'code':"stage_code", 'name':"stage_name"})
    try:
        tcList=sorted( list(df_tests['test_code'].unique()) )
    except TypeError:
        tcList=df_tests['test_code'].unique()
    for tc in tcList:
        if type(tc)!=type("str"):
            continue
        if "*" in tc:
            tabStr.append(f"### Test Code: __{tc}__")
        else:
            tabStr.append(f"### Test Code: **{tc}**")
        stageList=df_tests.query('test_code=="'+tc+'"')['stage_code'].to_list()
        stageListStr=", ".join(stageList)
        tabStr.append(f"stages: {stageListStr}")
        try:
            testInfo=myClient.get('getTestTypeByCode', json={'project':compInfo['project']['code'],'componentType':compInfo['code'],'code':tc}) 
        except itkX.BadRequest:
            tabStr.append("**NB** problem getting testType info.")
            continue

        # print(f"{tc}: {testInfo.keys()}")
        for pr in ['properties','parameters']:
            # print(f"{pr}")
            tabStr.append(f"\n**{pr}**\n")
            try:
                df_pr=pd.DataFrame(testInfo[pr])
                if "required" in df_pr.columns:
                    tabStr.append(df_pr[['name','code','description','dataType','valueType','required']].to_markdown(index=False))
                else:
                    tabStr.append("**NB** _required_ flag missing")
                    tabStr.append(df_pr[['name','code','description','dataType','valueType']].to_markdown(index=False))
            except KeyError:
                pass

    return tabStr


### batchType formatting
def GetBatchTypeTable(myClient,batchInfo):

    tabStr=[]
    df_bt=pd.DataFrame([batchInfo])
    df_bt['componentCodes']=df_bt['componentTypes'].apply(lambda x: [ct['code'] for ct in x] if type(x)==type([]) else None)
    tabStr.append(df_bt[['name','code','state','componentCodes']].to_markdown(index=False))
    try:
        n_batches= myClient.get('listBatches', json={'filterMap':{'batchType':df_bt['code'].to_list()}, 'pageInfo':{'pageSize':1} } ).total
        tabStr.append(f"Batches of this type: {n_batches}")
    except itkdb.exceptions.BadRequest:
        print("- can't get batch count")

    return tabStr


### cluster formatting
def GetClusterTable(clusInfo,myProjCode):

    tabStr=[]
    df_clus=pd.DataFrame(clusInfo)
    df_clus=df_clus.query(f'code.str.contains("{myProjCode}")')
    tabStr.append(df_clus[['name','code']].to_markdown(index=False))### categories
    tabStr.append(f"{len(df_clus.index)} clusters")
    df_clus['instituteList']=df_clus['instituteList'].apply(lambda x: [ct['code'] for ct in x] if type(x)==type([]) else None)

    tabStr.append("## Institutions per Cluster")
    for cc in df_clus['code'].unique():
        tabStr.append(f"### {cc}")
        instList=df_clus.query(f'code=="{cc}"')['instituteList'].values[0]
        tabStr.append(", ".join(instList))

    return tabStr
