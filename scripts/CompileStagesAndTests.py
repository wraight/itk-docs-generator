### imports
import os
import sys
import itkdb
import itkdb.exceptions as itkX
import pandas as pd
import argparse
import datetime
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonFunctions import *

###############
### Useful functions
###############

### clean up information
def Caster(x, toType):
    matchMap={'int':int,'float':float,'list':list,'dict':dict,'str':str}
    try:
        return matchMap[toType](x)
    except ValueError:
        return -1
    except IntCastingNaNError:
        return -1

### settings
def GetArgs(descStr=None):
    my_parser = argparse.ArgumentParser(description=descStr)
    # Add the arguments
    my_parser.add_argument('--project', '-p', type=str, help="ITk project code, i.e. S, P, CE or CM", required=True)
    my_parser.add_argument('--debug', '-d', type=bool, help="compile but don't update", default=False)
    # my_parser.add_argument('--ac1', '-1', type=str, help="PDB access token 1", required=True)
    # my_parser.add_argument('--ac2', '-2', type=str, help="PDB access token 2", required=True)

    args = my_parser.parse_args()
    return args


#############
### main part
#############

if __name__ == '__main__':

    ### commandline arguments
    args=GetArgs()

    print(vars(args))

    ### PDB Client
    myClient=SetClient()

    ### get project info.
    projList=myClient.get('listProjects', json={}) 
    print(projList.page_info)
    print(sorted(projList.data[0].keys()))
    df_proj=pd.json_normalize(projList.data, sep = "_")
    s = df_proj.apply(lambda x: pd.Series(x['subprojects']),axis=1).stack().reset_index(level=1, drop=True)
    s.name = 'subprojects'
    df_proj=df_proj.drop('subprojects', axis=1).join(s).reset_index(drop=True)
    df_proj.columns
    # display(df_proj.style.apply(ColourCells, df=df_proj, colName='name', flip=True, axis=1))
    print("### Projects info.")
    print(df_proj.to_markdown())

    ### get project componentType info.
    compTypeList=myClient.get('listComponentTypes', json={'project':args.project}) 
    print(compTypeList.page_info)
    print(sorted(compTypeList.data[0].keys()))

    ### loop over componentTypes to find stage and test information
    stageList=[]
    for x in compTypeList.data:
        if x['state']!="active": continue
        print(x['code'])
        if "stages" not in x.keys(): 
            print("No stages")
            stageList.append({'compType':x['code']})
            continue
        if x['stages']==None:
            print("Null stages")
            stageList.append({'compType':x['code']})
            continue 
        for st in x['stages']:
            print("\t"+st['code'])
            try:
                for tt in st['testTypes']:
                    print("\t"+"\t"+tt['testType'])
                    try:
                        ttVal=myClient.get('getTestType', json={'id':tt['testType']})
                        print("\t"+"\t"+"-->",ttVal['code'])
                        stageList.append({'compType':x['code'],'stage':st['code'],'stageOrder':st['order'],'altStage':st['alternative'],'testOrder':tt['order'],'testType':ttVal['code']})
                    except itkX.BadRequest:
                        print("\t"+"\t"+"-->","No testType found")
                        stageList.append({'compType':x['code'],'stage':st['code'],'stageOrder':st['order'],'altStage':st['alternative'],'testOrder':tt['order']})                    
            except KeyError:
                print("\t"+"-->","No testTypes")
                try:
                    stageList.append({'compType':x['code'],'stage':st['code'],'stageOrder':st['order'],'altStage':st['alternative']})
                except KeyError:
                    try:
                        stageList.append({'compType':x['code'],'stage':st['code'],'stageOrder':st['order']})
                    except KeyError:
                        stageList.append({'compType':x['code'],'stage':st['code']})
            except TypeError:
                print("\t"+"-->","typeError")
                stageList.append({'compType':x['code'],'stage':st['code']})

    ### clean up and display information
    df_stageList=pd.DataFrame(stageList)
    ordList=['compType']
    try:
        df_stageList['altStage']=df_stageList['altStage'].astype(str)
    except KeyError:
        pass
    try:
        df_stageList['stageOrder']=df_stageList['stageOrder'].apply(lambda x: Caster(x, "int"))#.astype(int)
        ordList.append('stageOrder')
    except KeyError:
        pass 
    try:
        df_stageList['testOrder']=df_stageList['testOrder'].apply(lambda x: Caster(x, "int"))
        ordList.append('testOrder')
    except KeyError:
        pass 
    df_stageList=df_stageList.sort_values(by=ordList)

    #######
    # Append datestamp to dataframe
    #######
    print("Add datestamp...")
    if not df_stageList.empty or len(df_stageList.columns)<1:
        dtDict={col:"None" for col in df_stageList.columns}
        dtDict[df_stageList.columns[0]]="Date Stamp"
        if len(df_stageList.columns)<2:
            # hack while CM underpopulated
            dtDict[df_stageList.columns[0]]="Date Stamp "+datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        else:
            dtDict[df_stageList.columns[1]]=datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
        print(dtDict)
        # concatonate
        df_stageList=pd.concat([df_stageList, pd.DataFrame(dtDict, index=[0])])
    else:
        print("- no columns to add stamp")
    ### save file
    outName="stageTestList_"+args.project+".csv"
    df_stageList.to_csv("outputs/"+outName,index=False)
    print("file saved")    
    
    #######
    # distribution
    #######

    ### get EOS stuff from environment
    eos_usr=CheckCredential("eos_usr")
    eos_pwd=CheckCredential("eos_pwd")

    # stop if eos info. missing
    if eos_pwd==None or eos_usr==None:
        print("EOS info. missing. Stop here.")
    else:
        print("Found EOS info. sending on.")
        print(f"send {outName}")
        if SendToEos(eos_usr, eos_pwd, "/eos/user/w/wraight/www/itk-pdb-structures/", "outputs/"+outName):
            print("Sent to eos successfully")
            remoteSave=True
        else:
            print("Something went awry")

