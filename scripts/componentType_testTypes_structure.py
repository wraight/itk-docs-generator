### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.2.4
import pandas as pd
# !{sys.executable} -m pip install itkdb==0.3.7
import itkdb
import itkdb.exceptions as itkX
from datetime import datetime
from string import ascii_lowercase 
### other
cwd = os.getcwd()
### local
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonDocs import *
from commonFunctions import *

#################
### functions
#################



#################
### main
#################
def main(projCode, compType=None):

    ### PDB Client
    myClient=SetClient()

    ### check project
    if projCode not in ["CM","CE","P","S"]:
        print(f"project code not recognise: {projCode}")
        return -1
    
    ### get project componentTypes
    if compType==None:
        print(f"Searching for {projCode} components")
        compList=[ct['code'] for ct in myClient.get('listComponentTypes', json={'project':projCode}).data]
        print(f"- {len(compList)} components found")
    else:
        compList=[compType]
        print(f"Using input componentType: {compType}")
    
    ### set output path
    writePath=cwd+"/outputs/"+projMap[projCode]+"-structures/testTypes/"
    if not os.path.isdir(writePath):
        os.makedirs(writePath, exist_ok=True)

    ### get components
    for compType in sorted(compList):
        print(f"working on: {compType}")
        compInfo=myClient.get('getComponentTypeByCode', json={'project':projCode,'code':compType}) 

        fileName=projCode+"_"+compType+"_testTypes.md"

        with open(writePath+fileName, "w") as text_file:
            ### intro
            text_file.write(f"# TestType Structure for {compInfo['name']} ({projCode}) \n\n")
            for d in Disclaimer():
                text_file.write(f"{d}\n\n")
            text_file.write("Tables generated from PDB componentType data-strucutre\n\n")
            
            ### tests
            text_file.write("\n\n## Tests Per Stage\n\n")
            try:
                for m in GetTestsPerStageTables(compInfo['stages']):
                    text_file.write(f"\n{m}\n")
            except KeyError:
                print(f"\nNo stages found\n")

            ### test details
            text_file.write("\n\n## Test Details\n\n")
            for p in GetTestTypeTable(myClient,compInfo):
                text_file.write(f"\n{p}\n")

            ### three empty lines
            text_file.write(f"\n\nend document\n\n\n")


if __name__ == "__main__":

    print(f"### In {__file__}")
    args=GetArgs('testType structure')

    print(vars(args))

    if args.projCode==None:
        print("No project code specified!")
        exit(0)

    if args.compType==None:
        main(args.projCode)
    else:
        main(args.projCode, args.compType)

    print("All done.")

