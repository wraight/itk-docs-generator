### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.2.4
import pandas as pd
# !{sys.executable} -m pip install itkdb==0.3.7
import itkdb
import itkdb.exceptions as itkX
from datetime import datetime
from string import ascii_lowercase 
### other
cwd = os.getcwd()
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonDocs import *
from commonFunctions import *


#################
### functions
#################
clusMap={'S':"STRIPS", 'P':"PIXELS", 'CM': "MECHANICS", 'CE':"ELECTRONICS"}

#################
### main
#################
def main(projCode, compType=None):

    ### PDB Client
    myClient=SetClient()

    ### check project
    if projCode not in ["CM","CE","P","S"]:
        print(f"project code not recognise: {projCode}")
        return -1

    ### get project componentTypes
    print(f"Searching for {projCode} clusters")
    clusInfo=myClient.get('listClusters', json={'project':projCode}).data
    print(f"- {len(clusInfo)} clusters found")

    
    ### set output path
    writePath=cwd+"/outputs/"+projMap[projCode]+"-structures/clusters/"
    if not os.path.isdir(writePath):
        os.makedirs(writePath, exist_ok=True)

        fileName=projCode+"_clusters.md"

        with open(writePath+fileName, "w") as text_file:

            ### intro
            text_file.write(f"# Clusters for {projCode} \n\n")
            for d in Disclaimer():
                text_file.write(f"{d}\n\n")
            text_file.write("Tables generated from PDB clusters data-strucutre\n\n")

            ### componentTypes and batch count
            text_file.write("\n\n## Info\n\n")
            for m in GetClusterTable(clusInfo,clusMap[projCode]):
                text_file.write(f"\n{m}\n")

            ### three empty lines
            text_file.write(f"\n\nend document\n\n\n")


if __name__ == "__main__":

    print(f"### In {__file__}")
    args=GetArgs('clusters structure')

    print(vars(args))

    if args.projCode==None:
        print("No project code specified!")
        exit(0)

    main(args.projCode)

    print("All done.")
