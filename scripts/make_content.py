### imports
import os
import sys
import json
from bs4 import BeautifulSoup
import requests
from datetime import datetime
import urllib.request

#####################
### useful functions
#####################

### find files

def GetLinks(url, snip="href", creationNote=False):
    reqs = requests.get(url)
    soup = BeautifulSoup(reqs.text, 'html.parser')

    links = []
    for link in soup.find_all('a'):
        # print(link)
        name=link.get(snip).replace('./','')
        # print(name)
        if "http://cern" in name or "https://cern" in name or"http://www.cern" in name  or "http://itssb" in name: # ignore standard cern stuff
            continue
        prefix=url
        if prefix[-1]!="/":
            prefix+="/"
        print(f"- got link for: {name}")
        links.append({'link':prefix+name, 'name':name, 'note':None})
        if creationNote:
            print(" - ... and the creation date...")
            links[-1]['note']=GetReportCreationData(prefix+name, "Made on","\\n\\n")
            print("- done.")
    return links

### Write files

### quick key of acronymns
def QuickKey():
    retStr=[]
    retStr.append("!!! labels ")
    retStr.append(f"\tcompType &rarr; componentType structure")
    retStr.append(f"\ttestType &rarr; testType structure")
    retStr.append(f"\txeno &rarr; infromation that lies _outside_ Production Database")
    return retStr


### intro page content
def IntroContent():
    ### line by line markdown code
    introArr=["## Welcome ITk PDB Structure Repository",
          "This is intended as a repository for information on from ITk Production Database object structures.",
          " - _Production Quality_ reporting is done elsewhere ... details TBC",
          "!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    introStr=""
    for ia in introArr:
        introStr+=f"{ia}\n\n"
    
    return introStr

### write WBSmarkdown page
def WriteWBSPage(wbs):
    
    print(f"Writing: {wbs['fileName']}")
    with open(wbs['fileName'], "w") as content_file:

        ### header 
        print("- header...")

        # WBS code and name
        if "code" in wbs.keys() and "name" in wbs.keys():
            content_file.write(f"# WBS {wbs['code']}: {wbs['name'].title()} \n\n")
        else:
            content_file.write(f"# Other reports \n\n")
        content_file.write("## Header Info. \n\n")
        # ACs
        content_file.write("### Responsible people \n" )
        if "ACs" in wbs.keys() and len(wbs['ACs'])>0:
            content_file.write("\n - "+"\n - ".join(sorted(wbs['ACs'])) )
        else:
            content_file.write("\n _no ACs defined_")
        content_file.write("\n\n" )
        # component types
        content_file.write("### Included component types \n" )
        if "CTs" in wbs.keys() and len(wbs['CTs'])>0:
            content_file.write("\n - "+"\n - ".join(sorted(wbs['CTs'])) )
        else:
            content_file.write("\n _no component types defined_")
        content_file.write("\n\n" )

### build WBS overview table
def BuildTableWBS(wbsList):
    #define string list
    tableArr=[]
    # table header
    tableArr.append("| code |  project | name |  ACs | #reports |")
    tableArr.append("| --- | --- | --- | --- | --- |")
    # table rows
    for wbs in wbsList:
        acStr=",".join(wbs['ACs'])
        tableArr.append(f"| {wbs['code']} | {wbs['project']} | {wbs['name']} | {acStr} | {len(wbs['reports'])} |")
    # return
    return tableArr

#####################
### main function
#####################
def main():

    ###################
    ### 1. find content: reports on EOS and compile to list
    ###################
    print("\n### Find reports")
    print("Finding reports and write pages")
    # reports in EOS in sub-directories
    url=f'https://wraight.web.cern.ch/itk-pdb-structures'
    mdList=[]
    for proj in ['pixels','strips','common-mechanics','common-electronics']:
        proj_url=url+f"/{proj}-structures/"
        print(f"\tworking on: {proj} \n\t- {proj_url}")
        proj_mds=GetLinks(proj_url) #, "href", False) 
        print(f"\t- found: {len(proj_mds)} pages")
        mdList.extend(proj_mds)
    print(f"Final count of files: {len(mdList)}")


    ###################
    ### 2. structure content: read in WBS list to match PDB componentTypes to WBS codes
    ###################
    print("\n### Match reports to WBS")
    wbsList=[]
    with open('wbsList.json') as json_file:
        data = json.load(json_file)
        # print(data)
        # formatting - add CT  and report lists
        wbsList=data['wbsList']

    for wbs in wbsList:
        # add extra keys
        for k in ["CTs", "reports"]:
            if k not in wbs.keys():
                wbs[k]=[]
        # codes-->projects
        if "2.1." in wbs['code']:
            wbs['project']="pixels"
            ctList=["_"+ct+"_" for ct in wbs['CTs']]
            wbs['reports']= [ md for md in mdList if "P_" in md['name'] and any(ct in md['name'] for ct in ctList) ] 
            # filter assigned reports
            mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbs['reports']], mdList))
        elif "2.2." in wbs['code']:
            wbs['project']="strips"
            ctList=["_"+ct+"_" for ct in wbs['CTs']]
            wbs['reports']= [ md for md in mdList if "S_" in md['name'] and any(ct in md['name'] for ct in ctList) ] 
            # filter assigned reports
            mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbs['reports']], mdList))
        elif "2.3." in wbs['code']:
            wbs['project']="common_mechanics"
            ctList=["_"+ct+"_" for ct in wbs['CTs']]
            wbs['reports']= [ md for md in mdList if "CM_" in md['name'] and any(ct in md['name'] for ct in ctList) ] 
            # filter assigned reports
            mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbs['reports']], mdList))
        elif "2.4." in wbs['code']:
            wbs['project']="common_electronics"
            ctList=["_"+ct+"_" for ct in wbs['CTs']]
            wbs['reports']= [ md for md in mdList if "CE_" in md['name'] and any(ct in md['name'] for ct in ctList) ] 
            # filter assigned reports
            mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbs['reports']], mdList))
        elif "2.5." in wbs['code']:
            wbs['project']="common items"
            # mdList= list( set(mdList) - set(wbs['reports']) )
        else:
            print(f"unknown code: {wbs['code']}")

    ### catch unassigned things
    wbsList.append({'project':"pixels", 'code':"2.1.X", 'name':"unassigned", 'nickName':"NA", 'reports':[md for md in mdList if "P_" in md['name']] } )
    mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbsList[-1]['reports']], mdList))
    wbsList.append( {'project':"strips", 'code':"2.2.X", 'name':"unassigned", 'nickName':"NA", 'reports':[md for md in mdList if "S_" in md['name']] } )
    mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbsList[-1]['reports']], mdList))
    wbsList.append( {'project':"common_mechanics", 'code':"2.3.X", 'name':"unassigned", 'nickName':"NA", 'reports':[md for md in mdList if "CM_" in md['name']] } )
    mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbsList[-1]['reports']], mdList))
    wbsList.append( {'project':"common_electronics", 'code':"2.4.X", 'name':"unassigned", 'nickName':"NA", 'reports':[md for md in mdList if "CE_" in md['name']] } )
    mdList = list(filter(lambda md: md['name'] not in [w['name'] for w in wbsList[-1]['reports']], mdList))

    ### non-assigned reports
    print("Report count:")
    totalAssigned=0
    for wbs in wbsList:
        print(f"- {wbs['code']} {wbs['nickName']}: {len(wbs['reports'])}") 
        totalAssigned+=len(wbs['reports'])
    print(f"Total assigned things: {totalAssigned}")
    if len(mdList)>1:
        print(f"Unassigned things: {len(mdList)}")
        print(mdList)
    else:
        print(" - all things assigned")


    ###################
    ### 3. write structure: docs directories
    ###################
    print("\n### Write directories")
    docsDir="docs/"

    ### write index (with timestamp)
    print("Writing index")
    with open(docsDir+"index.md", "a") as content_file:
        ### wbs summary table
        content_file.write(f"\n## WBS Structure \n")
        content_file.write("\ncf [__ITk Organigram__](https://atlaspo.cern.ch/internal/ATLASOrganisation/index.html?value=ITK)\n\n")
        content_file.write("\n".join(BuildTableWBS([wbs for wbs in wbsList if "NA" not in wbs['nickName']]))+"\n\n")
        ### quick summary of found reports
        content_file.write(f"\n### Component Summary\n\n")
        content_file.write(f"__Total reports compiled: {len(mdList)+totalAssigned}__\n\n") 
        for wbs in wbsList:
            content_file.write(f"- {wbs['project']} {wbs['code']} {wbs['nickName']}: {len(wbs['reports'])} \n")
        content_file.write(f"\n\nend document\n\n\n") 
        
    ### loop over wbsList
    for wbs in wbsList:
        # print(wbs)
        ### make directory if necessary
        if wbs['project'] not in [f.path.split('/')[-1] for f in os.scandir(docsDir) if f.is_dir() ]:
            print(f"no {wbs['project']} found")
            print(f"- making: {wbs['project']}")
            os.mkdir(docsDir+wbs['project']) 
        edName=wbs['code']+"_"+wbs['nickName'].replace(' ','_')
        if edName not in [f.path.split('/')[-1] for f in os.scandir(docsDir+wbs['project']) if f.is_dir() ]:
            print(f"no {edName} found")
            print(f"- making: {edName}")
            os.mkdir(docsDir+wbs['project']+"/"+edName) 
        ### write page        
        wbs['fileName']=docsDir+wbs['project']+"/"+edName+"/"+wbs['name']+" WBS.md"
        WriteWBSPage(wbs)
        ### write reports
        # wbs['fileName']=docsDir+wbs['project']+"/WBS_"+wbs['code'].replace('.','_')+".md"
        print(f"Get reports for: {edName}, {len(wbs['reports'])} found")
        for e,rep in enumerate(wbs['reports']):
            print(f" - {e} Get: {rep['link']}")
            try:
                urllib.request.urlretrieve(rep['link'], docsDir+wbs['project']+"/"+edName+"/"+"_".join(rep['name'].split('_')[1::]))
            except urllib.error.HTTPError:
                print("   - COULD NOT FIND THIS")


    ###################
    ### 4. use structure: update yml
    ###################
    print("### Update yml")
    with open("mkdocs.yml", "a") as mkdocs_file:
        # loop over directories
        for contDir in sorted([f.path for f in os.scandir(docsDir) if f.is_dir()]):
            dirDone=False
            if "images" in contDir:
                continue
            print(f"got dir: {contDir}")
            # write files
            for fileName in sorted([f.path for f in os.scandir(contDir) if f.is_file()]):
                print(f" - got file: {fileName}")
                label=fileName.split('/')[-1].replace('.md','')
                if label[0] in [' ','_']:
                    label=label[1::]
                if label[-1] in [' ','_']:
                    label=label[0:-1]
                if not dirDone:
                    mkdocs_file.write(f"  - {contDir.split('/')[-1]}: \n")
                    dirDone=True
                newStr=f"        - {label}: {fileName.replace(docsDir,'')}\n"
                mkdocs_file.write(newStr)
            for subDir in sorted([f.path for f in os.scandir(contDir) if f.is_dir()]):
                subDone=False
                print(f" - got sub-directory: {subDir}")
                for fileName in sorted([f.path for f in os.scandir(subDir) if f.is_file()]):
                    print(f"  - got file: {fileName}")
                    label=fileName.split('/')[-1].replace('.md','')
                    if label[0] in [' ','_']:
                        label=label[1::]
                    if label[-1] in [' ','_']:
                        label=label[0:-1]
                    if not dirDone:
                        mkdocs_file.write(f"  - {contDir.split('/')[-1]}: \n")
                        dirDone=True
                    if not subDone:
                        mkdocs_file.write(f"    - {subDir.split('/')[-1]}: \n")
                        subDone=True
                    newStr=f"      - {label}: {fileName.replace(docsDir,'')}\n"
                    mkdocs_file.write(newStr)




### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
