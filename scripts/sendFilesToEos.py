### get itkdb for PDB interaction
import os
import sys

from datetime import datetime
from string import ascii_lowercase 
import paramiko
from scp import SCPClient
from stat import S_ISDIR, S_ISREG
import smtplib 
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonDocs import *
from commonFunctions import *
### other
cwd = os.getcwd()

#################
### functions
#################



#################
### main
#################
def main(projCode, compType=None):

    ### check project
    if projCode not in ["CM","CE","P","S"]:
        print(f"project code not recognise: {projCode}")
        return -1
        
    ## get file paths from output path
    outPath=f"outputs/{projMap[projCode]}-structures/"
    fileList=[]
    for dirName in ['compTypes','testTypes','xenos']:
        localPath=outPath+dirName+"/"
        for fileName in sorted([f.path for f in os.scandir(localPath) if f.is_file() and ".md" in f.path]):
            fileList.append(fileName)

    ### filter filepaths for component type if necessary
    if compType!=None:
        fileList=[f for f in fileList if compType in f]

    ### print files
    print(f"FileList: \n{fileList}")

    ### get EOS stuff from environment
    eos_usr=CheckCredential("eos_usr")
    eos_pwd=CheckCredential("eos_pwd")


    # stop if eos info. missing
    if eos_pwd==None or eos_usr==None:
        print("EOS info. missing. Stop here.")
    else:
        print("Found EOS info. sending on.")
        destDir=projMap[projCode]+"-structures/"
        print(f"send to {destDir}")

        # SendToEos2(eos_usr, eos_pwd, "/eos/user/w/wraight/www/itk-pdb-structures/common-electronics-structures/", "outputs/common-electronics-structures/compTypes/CE_TEST_SENSOR_COMMON_compType.md")

        if SendFileListEos(eos_usr, eos_pwd, "/eos/user/w/wraight/www/itk-pdb-structures/"+destDir, fileList):
            print("Sent to eos successfully")
        else:
            print("Something went awry")



if __name__ == "__main__":

    print(f"### In {__file__}")
    args=GetArgs('Send to EOS')

    print(vars(args))

    if args.projCode==None:
        print("No project code specified!")
        exit(0)

    if args.compType==None:
        main(args.projCode)
    else:
        main(args.projCode, args.compType)

    print("All done.")
