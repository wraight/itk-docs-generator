### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.2.4
import pandas as pd
# !{sys.executable} -m pip install itkdb==0.3.7
import itkdb
import itkdb.exceptions as itkX
from datetime import datetime
from string import ascii_lowercase 
### other
cwd = os.getcwd()
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonDocs import *
from commonFunctions import *


#################
### functions
#################



#################
### main
#################
def main(projCode, compType=None):

    ### PDB Client
    myClient=SetClient()

    ### check project
    if projCode not in ["CM","CE","P","S"]:
        print(f"project code not recognise: {projCode}")
        return -1

    ### get project componentTypes
    print(f"Searching for {projCode} batches")
    batchList=[ct['code'] for ct in myClient.get('listBatchTypes', json={'project':projCode}).data]
    print(f"- {len(batchList)} batches found")

    
    ### set output path
    writePath=cwd+"/outputs/"+projMap[projCode]+"-structures/batchTypes/"
    if not os.path.isdir(writePath):
        os.makedirs(writePath, exist_ok=True)

    ### get components
    for batchType in sorted(batchList):
        print(f"working on: {batchType}")
        batchInfo=myClient.get('getBatchTypeByCode', json={'project':projCode,'code':batchType}) 
        
        fileName=projCode+"_"+batchType+"_batchType.md"

        with open(writePath+fileName, "w") as text_file:
            ### intro
            text_file.write(f"# BatchType Structure for {batchInfo['name']} ({projCode}) \n\n")
            for d in Disclaimer():
                text_file.write(f"{d}\n\n")
            text_file.write("Tables generated from PDB batchType data-strucutre\n\n")

            ### componentTypes and batch count
            text_file.write("\n\n## Info\n\n")
            for m in GetBatchTypeTable(myClient,batchInfo):
                text_file.write(f"\n{m}\n")

            ### tests
            text_file.write("\n\n## ComponentTypes\n\n")
            text_file.write(GetDictTable(batchInfo,'componentTypes'))

            ### properties
            text_file.write("\n\n## Properties\n\n")
            text_file.write(GetDictTable(batchInfo,'properties'))
            
            ### flags
            text_file.write("\n\n## Flags\n\n")
            text_file.write(GetDictTable(batchInfo,'flags'))

            ### tests
            text_file.write("\n\n## TestTypes\n\n")
            text_file.write(GetDictTable(batchInfo,'testTypes'))

            ### three empty lines
            text_file.write(f"\n\nend document\n\n\n")


if __name__ == "__main__":

    print(f"### In {__file__}")
    args=GetArgs('batchType structure')

    print(vars(args))

    if args.projCode==None:
        print("No project code specified!")
        exit(0)

    main(args.projCode)

    print("All done.")
