### get itkdb for PDB interaction
import os
import sys
# !{sys.executable} -m pip install pandas==1.2.4
import pandas as pd
# !{sys.executable} -m pip install itkdb==0.3.7
import itkdb
import itkdb.exceptions as itkX
from datetime import datetime
from string import ascii_lowercase 
import json
import pprint
### other
cwd = os.getcwd()
### local
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonDocs import *
from commonFunctions import *


#################
### functions
#################



#################
### main
#################
def main(projCode, compType=None):

    ### PDB Client
    myClient=SetClient()

    ### check project
    if projCode not in ["CM","CE","P","S"]:
        print(f"project code not recognise: {projCode}")
        return -1
    
    ### get project componentTypes
    if compType==None:
        print(f"Searching for {projCode} components")
        compList=[ct['code'] for ct in myClient.get('listComponentTypes', json={'project':projCode}).data]
        print(f"- {len(compList)} components found")
    else:
        compList=[compType]
        print(f"Using input componentType: {compType}")
    
    ### set output path
    writePath=cwd+"/outputs/"+projMap[projCode]+"-structures/xenos/"
    if not os.path.isdir(writePath):
        os.makedirs(writePath, exist_ok=True)
    
    ### get components
    for compType in compList:
        print(f"working on: {compType}")

        fileName=projCode+"_"+compType+"_xeno.md"

        with open(writePath+fileName, "w") as text_file:
            ### intro
            text_file.write(f"# Other Information for {compType} ({projCode}) \n\n")
            for d in Disclaimer():
                text_file.write(f"{d}\n\n")
            text_file.write("Attempting to cross-reference PDB componentType\n\n")

            ### EDMS links
            text_file.write("\n\n## EDMS links\n\n")
            text_file.write("__TODO__")

            ### twikis
            text_file.write("\n\n## twiki\n\n")
            # text_file.write("__TODO__\n\n")
            # map from PDB to twiki
            mapCT=next((item for item in crossDict[projCode] if item['PDB']==compType), None)
            if mapCT==None or mapCT['twiki']==None:
                print(f"### mapping has failed for {compType}. Try without")
                text_file.write("No link found.")
            else:
                twiki_link="https://twiki.cern.ch/twiki/bin/view/Atlas/"+mapCT['twiki']
                text_file.write(f"[{twiki_link}]({twiki_link})")

            ### Other stuff
            text_file.write("\n\n## Other\n\n")
            text_file.write("__TODO__")

            ### three empty lines
            text_file.write(f"\n\nend document\n\n\n")
        


if __name__ == "__main__":

    print(f"### In {__file__}")
    args=GetArgs('xeno page')

    print(vars(args))

    if args.projCode==None:
        print("No project code specified!")
        exit(0)

    if args.compType==None:
        main(args.projCode)
    else:
        main(args.projCode, args.compType)

    print("All done.")
