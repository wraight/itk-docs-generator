### get itkdb for PDB interaction
import gitlab
import pandas as pd
import plotly.graph_objects as go
import base64
import os
import sys
### other
cwd = os.getcwd()
### local
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/commonCode')
from commonDocs import *
from commonFunctions import *


#################
### functions
#################
### define recursive function to dig through directory tree
def RecCheck(dictObj, project, branch, keepList):
    if dictObj['type']=="blob":
        print("keeping name:",dictObj['name'])
        keepList.append({
            'type':dictObj['type'], 
            'name':dictObj['name'].split('.')[0]
        })
        try:
            keepList[-1]['ext']=dictObj['name'].split('.')[1]
        except IndexError:
            keepList[-1]['ext']=None
        try:
            keepList[-1]['parDir']="/".join(dictObj['path'].split('/')[:-1])
        except IndexError:
            keepList[-1]['parDir']=None
    elif dictObj['type']=="tree":
        keepList.append({
            'type':dictObj['type'], 
            'name':dictObj['path'].split('/')[-1],
            'ext':"dir"
        })
        try:
            keepList[-1]['parDir']="/".join(dictObj['path'].split('/')[:-1])
        except IndexError:
            keepList[-1]['parDir']=None
        for x in project.repository_tree(path=dictObj['path'], ref=branch):
            RecCheck(x, project, branch, keepList)
    else:
        print("Didn't expect that:", dictObj['type'])

def GetRepoList(project, branch, initialPath):
    ### loop through tree
    keepList=[]
    # RecCheck({'type':"tree",'path':""},"top",keepList)
    RecCheck({'type':"tree",'path':initialPath},project,branch,keepList)
    # pd.DataFrame(keepList)

    ### tidy up list
    df_repo=pd.DataFrame(keepList)
    df_repo['name']=df_repo.apply(lambda row: row['ext'] if row['name']=='' else row['name'], axis=1)
    df_repo['parDir']=df_repo['parDir'].apply(lambda x: "top" if x=='top/' else x)
    parDirVals=list(df_repo['parDir'].unique())
    df_repo['parDirInd']=df_repo['parDir'].apply(lambda x: parDirVals.index(x))
    #df_repo=df_repo.reset_index()
    # build link
    urlPart="https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/tree/"
    df_repo['link']=df_repo.apply(lambda row: branch+"/"+"/".join(row['parDir'].split('/'))+"/"+row['name'], axis=1)
    df_repo['link']=df_repo.apply(lambda row: row['link']+"."+row['ext'] if row['type']=="blob" else row['link'], axis=1)
    df_repo['link']=df_repo['link'].apply(lambda x: urlPart+"/"+x.replace('//','/') )
    # df_repo.query('parDir.str.contains("pixels") & name=="README" | name=="readme"').reset_index(drop=True)#.to_csv("readmes.csv",index=False)
    return df_repo

#################
### main
#################
def main(projCode):
    ### set up gitLab credentials

    ### set up Kenny's credentials (ignore if user!=Kenny)
    credDir=os.getcwd()
    if os.path.isdir(credDir):
        print("directory found:",credDir)
        sys.path.insert(1, credDir)
        import myDetails
        credDict=myDetails.GetPDSCredentials()
        #print(credDict)
        gl = gitlab.Gitlab(url=credDict['url'], private_token=credDict['token'], api_version=4)
        project = gl.projects.get(credDict['id'])
        print("done.")
    else:
        print("no directory found:",credDir)

    ### settings
    if projCode=="S":
        print(f"strips recognised: {projCode}")
        myProjCode="S"
        compList=['HCC','SENSOR','HYBRID_ASSEMBLY','PWB','MODULE','STAVE','BT']
    elif projCode=="P":
        print(f"pixels recognised: {projCode}")
        myProjCode="P"
        compList=['FE_CHIP','SENSOR_TILE','PCB','MODULE_CARRIER','BARE_MODULE','MODULE']
    else:
        print(f"project code not recognise: {projCode}")
        return -1
    
    ### set output path
    writePath=cwd+"/outputs/repoMaps/"
    if not os.path.isdir(writePath):
        os.mkdir(writePath) 

    #######################
    ### loop over componentTypes
    #######################
    for compType in compList:
        ## p_d_s
        branch = "master"
        directory = None
        if myProjCode=="P":
            directory="pixels"
        elif myProjCode=="S": 
            directory="strips"
        else:
            print(f"project code unknown: {myProjCode}. exiting")
        
        # map from PDB to p_d_s
        mapCT=next((item for item in crossDict[myProjCode] if item['PDB']==compType), None)
        if mapCT==None:
            print(f"### mapping has failed for {compType}. Try without")
            directory+="/"+compType.lower()+"s"
        else:
            directory+="/"+mapCT['p_d_s'].lower()
        
        ### get repository list
        df_repo=GetRepoList(project, branch, directory)
        # print(df_repo.to_markdown(index=False))

        if df_repo.empty:
            print(f"no files found for directory: {directory}")
            print(f"No matching directories for componentType: {compType}")
            continue

        fileName="repoMap_"+myProjCode+"_"+compType+".md"

        with open(writePath+fileName, "w") as text_file:
            ### intro
            text_file.write(f"# GitLab p_d_s repository map for {compType} \n\n")
            for d in Disclaimer():
                text_file.write(f"{d}\n\n")
            text_file.write("Tables generated from gitLab repo. structure\n\n")
            for d in PDSDisclaimer():
                text_file.write(f"{d}\n\n")
            
            ### list with links
            df_doc=df_repo 
            text_file.write(f"\n## **p_d_s** directories under {directory}\n\n")
            df_doc['fileName']=df_doc.apply(lambda row: row['name']+"."+row['ext'] if row['ext']!="dir" else row['name']+" ("+row['ext']+")", axis=1)
            df_doc['link']=df_doc['link'].apply(lambda x: x.replace("tree//","tree/") )
            df_doc['link']=df_doc['link'].apply(lambda x: "[link]("+x+")" )
            text_file.write(df_doc[['fileName','parDir','link']].to_markdown(index=False))
            if len(df_doc)==1:
                text_file.write("\n\n__Comment__: Not many entries - probably no corresponding repository in **p_d_s**.")

            ### get file content of readme
            text_file.write(f"\n\n### READMEs\n\n")
            df_rms=df_doc.query('name=="README"')
            if df_rms.empty:
                text_file.write("No README files found in this area.")
            else:
                text_file.write(df_rms[['fileName','parDir','link']].to_markdown(index=False))

            ### group directories
            text_file.write("\n\n### Pages per Directory\n\n")
            df_repo['subPages']=1
            df_repo_grp=df_repo.groupby(['parDir']).agg({'subPages':"sum"}).reset_index()
            # rough calculation to get level
            df_repo_grp['level']=df_repo_grp['parDir'].apply(lambda x: x.count('/') if len(x)>0 else -1)
            df_repo_grp=df_repo_grp.sort_values(by=['level'])
            text_file.write(df_repo_grp.to_markdown(index=False))

            ### map
            text_file.write("\n\n### Directory Map\n\n")
            ### seperate directory path into levels
            df_levels=df_repo[['name','type','parDir']]
            for i in range(0,10,1): # 10 should do
                df_levels['level'+str(i)]=df_levels['parDir'].apply(lambda x: x.split('/')[i] if len(x.split('/'))>i else None)
            df_levels.dropna(how='all', axis=1, inplace=True) # drop unused
            df_levels=df_levels.query('type=="blob"').reset_index(drop=True)
            df_levels['value']=1
            if df_levels.empty:
                text_file.write("no map required")
                continue

            ### use levels to build flow structure
            # start at low levels and work back to identify end points
            # loop over columns (top->bottom) to build connection to endpoints
            # at the moment: multiple lines at higher levels as repeeted connections
            text_file.write("\n\n``` mermaid ")
            text_file.write("\nflowchart TD ")

            colList=[col for col in df_levels.columns if "level" in col]
            for colz in colList[::-1]:
                for z in df_levels[colz].unique():
                    if z==None:
                        continue
                    df_z=df_levels.query(colz+'=="'+z+'"')
                    colStr=""
                    lineStr=""
                    for col in colList[0:colList.index(colz)]:
                        colStr+=df_z[col].values[0]
                        colStr=colStr.replace(' ','_')
                        lineStr+=f"{colStr}[{df_z[col].values[0]}]-->"
                        colStr+="_"
                    colStr+=df_z[colz].values[0]
                    colStr=colStr.replace(' ','_')
                    lineStr+=f"{colStr}[{df_z[colz].values[0]}]"
                    text_file.write("\n"+lineStr+" ")

            text_file.write("\n\n```")

            #######
            # distribution
            #######

            ### get EOS stuff from environment
            eos_usr=CheckCredential("eos_usr")
            eos_pwd=CheckCredential("eos_pwd")

            # stop if eos info. missing
            if eos_pwd==None or eos_usr==None:
                print("EOS info. missing. Stop here.")
            else:
                print("Found EOS info. sending on.")
                print(f"send {fileName}")
                if SendToEos(eos_usr, eos_pwd, "/eos/user/w/wraight/www/itk-pdb-structures/"+fileName, writePath+fileName):
                    print("Sent to eos successfully")
                    remoteSave=True
                else:
                    print("Something went awry")


if __name__ == "__main__":

    print(f"### In {__file__}")
    args=GetArgs('testType structure')

    print(vars(args))

    if args.projCode==None:
        print("No project code specified!")
        exit(0)

    main(args.projCode)

    print("All done.")

