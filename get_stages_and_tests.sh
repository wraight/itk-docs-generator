### loop over projects
echo "### Make project stage and test CSVs"
date

## check for project argument
projCode=NOWT
# compTypeCode=NOWT
while test $# -gt 0
do
    echo "-- check "$1
    case $1 in
    -pc|--projCode) projCode=$2 ; shift ;;
   #  -ctc|--compTypeCode) compTypeCode=$2 ; shift ;;
    esac
    shift
done

## check if projCode in list
declare -a arr=("CE" "CM" "P" "S")
match=false
echo "-- check $projCode in project list"
if [[ "${arr[@]}" =~ $projCode ]]; then
    # yes, list include item
    echo "-- matched project code"
    match=true
  else
    echo "-- no matching project code found"
fi
## if found then update list
if [ "$match" = true ] ; then
   declare -a arr=("$projCode")
fi

## now loop through the above array
for i in "${arr[@]}"
do
   echo "## working on $i"
   python scripts/CompileStagesAndTests.py --proj $i
done

echo "done"
