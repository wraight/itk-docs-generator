# itk-docs-generator

![xkcd](https://imgs.xkcd.com/comics/code_lifespan.png)

cite: https://xkcd.com/2730

Python scripts to generate markdown pages for [ITk-docs](https://itk.docs.cern.ch)

Powered by:
- [itkdb](https://itkdb.docs.cern.ch/latest/)
- [pandas](https://pandas.pydata.org)


## Overview

Run scripts in repository top directory. 
Scripts found in _scripts_ directory.
Outputs written to _outputs_ directory.

Required python packages in _requirements.txt_ file


## Notebooks

Example notebooks to walk through/develop scripts.


## Scripts

Scripts include:
- componentType structure script: tables and diagram
    - generate page with properties, flags,  stages,relatives

- testTypes structure script: tables and diagram
    - generate page with tests per stage, parameters & properties per test

- componentType xeno: links from outside PDB
    - if possible, collate EDMS, twiki and other links

- p_d_s repoMap: links
    - if possible, collate available scripts in p_d_s for componentType


## Run examples

Run from repository top directory.
 - options:
    | argument | meaning | 
    |:---|:---|
    | -h, --help | show  help message | 
    | --projCode, -p | project: S, P, CE, CM | 
    | --compType, -ct | componentType | 


Running componentType structure script, e.g. for pixels
> python scripts/componentType_compTypes_structure.py -p P

Running testType structure script, e.g. for strips
> python scripts/componentType_testTypes_structure.py -p S
